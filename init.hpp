//
//  init.hpp
//  R_P
//
//  Created by Eugene Yurin on 29.07.2020.
//  Copyright © 2020 Eugene Yurin. All rights reserved.
//

#pragma once

#include <math.h>

namespace input{
    void init() {
        using namespace properties;
        t = 0.0;
        r = 1.0;
        v = 0.0;
        r_ = r;
        v_ = v;
        //gas
        c_gas = sqrt(k*R/M*(liquid::T));
        //liquid
        liquid::calc();
        
        type Rcr = 1.0/2.0/pi/nu*sqrt(3*k*liquid::p/liquid::density);
        if (R0 != R0)
            R0 = Rcr * R0_Rcr;
        cout << "R0 = " << R0*1.0e6 << " μm" << endl;
        om = 2 * pi * nu * sqrt(liquid::density*R0*R0*R0 / liquid::sigma);
        
        //cout << "speed = " << liquid.c << endl;
//        liquid.mu = 10.0e-4;
//        liquid.rho = 1000.0;
//        sigma = 0.0715;
        
        c = (sqrt(liquid::sigma/liquid::density/R0))/liquid::c;
        c_gas = (sqrt(liquid::sigma/liquid::density/R0))/c_gas;
        
        
        
        //type tm = 1/(sqrt(sigma/liquid.rho/R0/R0/R0));
        PA = Pnu*R0/liquid::sigma;
        Pinf = liquid::p*R0/liquid::sigma;
        if (P0 == P0)
            P0 *= R0/liquid::sigma;
        else
            P0 = 2 + Pinf;
        mu = liquid::mu/sqrt(liquid::sigma*R0*liquid::density);
        M_liq = 0.0;
        M_gas = 0.0;
    }
}
