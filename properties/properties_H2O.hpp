//
//  properties_H2O.hpp
//  R_P
//
//  Created by Eugene Yurin on 20.12.2020.
//  Copyright © 2020 Eugene Yurin. All rights reserved.
//

#pragma once
namespace properties::kernel::H2O{
void calc(){
    using namespace properties::liquid;
    type c_sum = 1.0;
    type w = sqrt(1.0-T/647.096);
    
    
    if (sigma != sigma) {//surface tension
        sigma = 235.8e-3*pow((1 - T/647.096),1.256)*(1.0 - 0.625*(1 - T/647.096));
    }
    
    
    if (density != density){//density
        density = 0.0;
        type A[12] {6.072746418, -149.9456089, 2121.007381, -17164.83301, 90176.84684, -323045.6094, 805443.5472, -1398515.305, 1658696.365, -1281195.416, 580741.4278, -117181.9097};
        for (auto a: A) {
            density += a*c_sum;
            c_sum *= w;
        }
        density *= 322.0;
    }
    
    if (mu != mu){//viscosity
        type B[11] {0.945822019, -3.50567042, 23.61506749, -176.2217806, 960.5599634, -3533.866457, 8609.716158, -13637.7187, 13435.90472, -7453.116048, 1776.272412};
        c_sum = 1.0;
        mu = 0.0;
        for (auto b: B) {
            mu += b*c_sum;
            c_sum *= w;
        }
        mu = 3.95e-5/mu;
    }
    
    if (c != c){//sound speed
        type C[4] {-931, 15.412, - 0.0277, 1.113e-5};
        c = 0.0;
        c_sum = 1.0;
        for (auto ci: C) {
            c += ci*c_sum;
            c_sum *= T;
        }
    }
}
}
