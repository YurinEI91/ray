//
//  properties.hpp
//  R_P
//
//  Created by Eugene Yurin on 20.12.2020.
//  Copyright © 2020 Eugene Yurin. All rights reserved.
//

#pragma once

namespace properties{
namespace liquid{
    enum{
        water,
        glycerol
    }sub;
    double p;
    double t;
    double T;
    double c = NAN;   //sound speed
    double density = NAN;
    double sigma = NAN;   //surface tension
    double mu = NAN;  //viscosity
    void calc();
    }
    namespace vapor{
    double dens(){
        return NAN;
    }
    }
    namespace gas{
    double dens(){
        return NAN;
    }
    namespace mix{
    
    }
}
}
#include "properties_H2O.hpp"
#include "properties_GLY.hpp"

namespace properties{
namespace liquid{
void calc(){
    switch(sub){
        case water:
            kernel::H2O::calc();
            break;
        case glycerol:
            kernel::GLY::calc();
            break;
    }
}
}
}
