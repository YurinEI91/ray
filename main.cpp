//
//  main.cpp
//  R_P
//
//  Created by Eugene Yurin on 27.10.2019.
//  Copyright © 2019 Eugene Yurin. All rights reserved.
//
#include "header.hpp"

type dyn_h(type r){
    if (!smart_step)
        return h;
    if (r < 1)
        return h*r;
    return h/r;
}

type Pi(type r, type v, type t) {
//    T_T0 = pow(r,3-3*gam)*1.0;
//    M_gas = v*c_gas/sqrt(T_T0);
//    type dp = 0.0;
//    if (M_gas < 0)
//        dp = M_gas*M_gas/4;
    return
    - Pinf + PA*sin(om*(t + model::sou*r*c)) - model::sig*2/r - model::vis*4*mu*v/r + P0*pow(r, -3*gam)/**(1 + dp)*/;
}

type calc_a(type r, type v, type t) {
    //KM-Prospretty
    return (
            -1.5*(1 - model::sou*c*v/3)*v*v + (1 + model::sou*c*v)*Pi(r, v, t)
            -model::sou*3*gam*P0*c*v*pow(r, -3*gam)
            +model::sou*model::sig*2*c*v/r
            +model::sou*model::vis*4*c*v*v/r*mu
            )/(r*(1 - model::sou*model::sou*c*v)+model::vis*4*c*mu);
    //
//    return (
//            -1.5*(1 - c*v/3)*v*v + (1 + c*v)*Pi(r, v, t)
//            + (1-c*v)*(
//                    -3*gam*P0*c*v*pow(r, -3*gam)
//                    +2*c*v/r
//                    +4*c*v*v/r*mu
//                  )
//            )/(r*(1 - c*v)+4*c*mu*(1-c*v));
    /*
     return (
             -1.5*(1 - c*v/3)*v*v - (1 + c*v)*Pi(r, v, t)
             + (1 - c*v)*c*v*(-2/r - 3*gam*P0*pow(r, -3*gam))
             -3*gam*P0*c*v*pow(r, -3*gam)//todo
             +2*c*v/r//todo
             +4*c*v/r*mu//todo
             //-4*c*mu*a//todo
             )/r/(1 - c*v);
     */
}

type calc_r(type v, type h) {
    return
        r_ + 0.5*h*(v_ + v);
}

void calc(type h) {
    r_ = r;
    v_ = v;
    f_ = f;
    for (unsigned i = 0; i < N_STEP; ++i) {
        f = calc_a(r, v, t + h);
        v = v_ + 0.5*h*(f_ + f);
        r = r_ + 0.5*h*(v_ + v);
    }
    if (v*v_ < 0 || r != r || v != v || r < 0) {
        r = r_;
        v = 0.0;
        type H = 0.2*h;
        for (int i = 0; i < N_STEP*5; ++i) {
            r = 0.0*r + 1.0*calc_r(0, H);
            f = calc_a(r, 0.0, t + H);
            H = 0.5*H-0.5*2*v_ / (f_ + f);
        }
        r_ = r;
        v_ = v;
        f_ = f;

        t_T = t*om/2/pi;
        T_T0 = pow(r,3-3*gam)*1.0;
        P_Pinf = P0/Pinf*pow(r, -3*gam);
        pa = (Pinf - PA*sin(om*t))/Pinf*properties::liquid::p*1.0e-5;
        if (r > RMAX)
            RMAX = r;
        output();

        for (unsigned i = 0; i < N_STEP; ++i) {
            f = calc_a(r, v, t + h);
            v = v_ + 0.5*(h - H)*(f_ + f);
            r = r_ + 0.5*(h - H)*(v_ + v);
        }
        if (H > h || H < 0)
            exit(2);
    }
    else if (f*f_ <= 0){
        output();
    }
    if (r != r || v != v || r < 0)
        exit(1);

    M_liq = v*c;
    M_gas = v*c_gas/sqrt(T_T0);
    t += h;
}

void calc(type n, int m, type k = 0.0) {
    if (m < 1)
        m = 1;
    n -= k;
    type T = n * 2 * pi / om;
    
    type Tk = k * 2 * pi / om;
    output.make = false;
    while (t < Tk)
        calc(dyn_h(r));
    output.make = true;
    RMAX = 0.0;
    t = 0.0;
    while (t < T) {
        t_T = t*om/2/pi;
        T_T0 = pow(r,3-gam*3);
        P_Pinf = P0/Pinf*pow(r, -3*gam);
        pa = (Pinf - PA*sin(om*t))/Pinf*properties::liquid::p*1.0e-5;
        output();
        for (int j = 0; j < m; j++)
        calc(dyn_h(r));
    }
}


int main(int argc, const char * argv[]) {
    if (argc == 1) {
        cout << "\nDEBUG\n";
        if (input::extract(DBG_COUNT, DBG_LINE))
            return 1;
    }
    else
    {
        if (input::extract(argc, argv))
            return 1;
    }
    input::init();
    
    calc(totalT, out_m, hidenT);
    output.calc_end = true;
    //cout << "R_MAX = " << RMAX << endl;
    ofstream file_r(string(PATH) + "r0.txt",ios::app);
    file_r << input::R0_Rcr << '\t' << RMAX << endl;
    file_r.close();
    return 0;
}
