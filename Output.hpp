//
//  output.hpp
//  R_P
//
//  Created by Eugene Yurin on 29.10.2019.
//  Copyright © 2019 Eugene Yurin. All rights reserved.
//
#pragma once

using namespace std;

class Output {
private:
    ofstream file;
    string path;
    vector<type*> collection;
public:
    bool plotpy;
    bool make;
    bool calc_end;
    Output(vector<type*> collection_,string head = "" , string path_ = "output.txt")
    :path (PATH + path_), plotpy(false), calc_end(false) {
        make = true;
        collection = collection_;
        file.open(path);
        file << head << endl;
    }
    ~Output() {
        file.close();
        if (plotpy && calc_end) {
            string command = string("python ") + PATH + string("plot.py");
            system(command.c_str());
        }
    }
    void operator()() {
        if (make) {
            for (auto c: collection)
                file << *c << '\t';
            file << endl;
        }
    }
};
