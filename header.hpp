//
//  header.hpp
//  R_P
//
//  Created by Eugene Yurin on 29.10.2019.
//  Copyright © 2019 Eugene Yurin. All rights reserved.
//
#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <string>

#ifdef __APPLE__
    #define PATH "/Users/eugeneyurin/Desktop/"
#else
    #define PATH "C:/Users/Алексей/Desktop/"
#endif
#define pi (3.1415926536)
#define R (8.31446261815324)
#define N_STEP (10)
#define KELVIN (273.15)

#define DBG_COUNT 11
const char * DBG_LINE[DBG_COUNT]{"","t=20","p=0.10","nu=21","Pn=0.12","R0/Rcr=0.14","plotpy","totalT=2","hidenT=0","out_m=1","gamma=1"};
typedef long double type;

//type min(type a, type b){
//    if (a < b)
//        return a;
//    return b;
//}
#include "output.hpp"

namespace input{
    type nu;
    type Pnu;
    type R0_Rcr;
}

type gam = 1.4;
type k = 1.4;
type M = 28.96e-3;

type r, r_;
type v, v_;
type t, h, h0;
type PA, Pinf;
type P0 = NAN;

type f_ = 0.0;
type f = 0.0;

type t_T;
type T_T0;
type P_Pinf;
type c;
type c_gas;
type mu;
type om;
type R0 = NAN;
type RMAX = 0.0;
type totalT;
type hidenT;
type M_liq;
type M_gas;
type pa;
unsigned out_m;
bool smart_step = false;
Output output(vector<type*>{&t_T,&pa, &r, &M_liq, &M_gas, &T_T0, &P_Pinf}, "$t$/T\t$pa\tR/R$_{0}$\tM$_{liq}$\tM$_{gas}$\tT/T$_{0}$\tp/p$_{0}$");//"t/T\tR/R$_{0}$\tP/P$_{0}$log"


namespace model{
unsigned sou = 1;
unsigned vis = 1;
unsigned sig = 1;
}

#include "properties/properties.hpp"
#include "init.hpp"
#include "extract.hpp"


