//
//  extract.hpp
//  R_P
//
//  Created by Eugene Yurin on 29.07.2020.
//  Copyright © 2020 Eugene Yurin. All rights reserved.
//

#pragma once

#include <string>
#include <vector>

namespace input{
    bool extract(const string &key, string &word){
        return (word.find(key) != string::npos);
    }
    bool extract(const string key, vector<string> &words){
        for (auto &word: words) {
            if (extract(key, word))
                return true;
        }
        return false;
    }
    void extract(const string &key, string &word, string &subs) {
        if (extract(key, word)){
            auto begin = word.find('=');
            if (begin != string::npos)
                subs = word.substr(begin + 1, word.size() - begin).c_str();
        }
    }
    bool extract(const string key, vector<string> &words, string &subs){
        for (auto &word: words) {
            extract(key, word, subs);
            if (!subs.empty())
                return true;
        }
        return false;
    }
    template <typename T>
    bool extract(const string key, string &word, T &val, const string post, const type coef){
        string subs;
        extract(key, word, subs);
        if (!subs.empty()) {
            val = atof(subs.c_str());
            cout << key << " = " << val << post << endl;
            val *= coef;
            return true;
        }
        return false;
    }
    template <typename T>
    void extract(const string key, vector<string> &words, T &val, const string post = "", const type coef = 1.0){
        for (auto &str: words)
            if(extract(key, str, val, post, coef))
                break;
    }
    int extract(int argc, const char * argv[]){
        using namespace properties;
        if (argc < 2)
            return 1;
        liquid::t = 20.0;
        liquid::p = 1.0e5;
        nu = 21.0e3;
        Pnu = 1.2e5;
        R0_Rcr = 0.14;
        totalT = 20;
        hidenT = 19;
        out_m = 100;
        h = 5.0e-4;

        vector<string> words(argc - 1);
        for (int i = 1; i < argc; ++i)
            words[i - 1] = string(argv[i]);
        
        string name;
        if (extract("file", words, name)) {
            cout << "file " << name << endl;
            ifstream inf(PATH + name);
            if (inf.is_open()) {
                string word;
                words.resize(0);
                while(inf >> word)
                    words.push_back(word);
                inf.close();
            }
            else
                return 1;
        }
        
        properties::liquid::sub = properties::liquid::water;
        
        if (extract("GLY", words)){
            properties::liquid::sub = properties::liquid::glycerol;
            cout << "glycerol\n";
        }
        extract("t", words, liquid::t, " °C", 1.0);
        extract("p_inf", words, liquid::p, " MPa", 1.0e6);
        extract("p_0", words, P0, " MPa", 1.0e6);
        extract("nu", words, nu, " kHz", 1.0e3);
        extract("Pn", words, Pnu, " MPa", 1.0e6);
        extract("R0/Rcr", words, R0_Rcr);
        extract("r0", words, R0, " μm", 1.0e-6);
        extract("totalT", words, totalT);
        extract("hidenT", words, hidenT);
        extract("out_m", words, out_m);
        extract("gamma", words, gam);
        extract("rho", words, liquid::density, " kg/m^3");
        extract("sigma", words, liquid::sigma, " N/m");
        extract("sound", words, liquid::c, " m/s");
        extract("k", words, k);
        extract("step", words, h);

        output.plotpy = extract("plotpy", words);
        if (extract("no_sig",words))
            model::sig = 0;
        if (extract("no_sou",words))
            model::sou = 0;
        if (extract("no_vis",words))
            model::vis = 0;
        smart_step = extract("smart_step", words);
        liquid::T = liquid::t + KELVIN;
        return 0;
    }
}
