import matplotlib.pyplot as plt
tab = []
PATH = '/Users/eugeneyurin/Desktop/'
with open(PATH + 'output.txt') as f:
	for line in f:
		l = line.split()
		if len(tab) == 0:
			for i in range(len(l)):
				loc = []
				loc.append(l[i])
				tab.append(loc)
		else:
			for i in range(len(l)):
				tab[i].append(float(l[i]))
	plt.rc('grid', linestyle='-', color='gray')
x = tab[0][0]
plt.xlabel(x)
tab[0].pop(0)
for i in range(1, len(tab)):
	y = tab[i][0]
	tab[i].pop(0)
	if 'log' in y:
		plt.yscale('log')
		y = y.replace('log','')
	plt.ylabel(y)
	plt.rc('grid', linestyle='-', color='gray')
	plt.grid(True)
	#plt.xlim(98.000000, 100.000000)
	plt.plot(tab[0], tab[i], color='black')
	plt.savefig(PATH + 'output'+str(i)+'.png')
	plt.cla()
for i in range(1, len(tab)):
	print(str(i) + ':\t' + str(min(tab[i])) + '\t' + str(max(tab[i])))
